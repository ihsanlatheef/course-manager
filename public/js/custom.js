if ($('.user-form').is(':visible')){
	$('.li-login').hide();
	$('.user-links').hide();
} else {
	$('.li-login').show();
	$('.user-links').show();
	$('.home').text('Home');
}

// Download to excel file
$(document).ready(function() {
  $("#toExcel").click(function(e) {
    e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('search-results-table');
    var new_table = $(table_div).clone();

	$(new_table).find('th').eq(10).remove();
	$(new_table).find('.to-be-removed').remove();

    var table_html = jQuery(new_table)[0].outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
    a.click();
  });
});

// Update course state when dropdown selection changes
// $(document).on('change', '.dynamic-course-state', function(e) {
// 	var empId = $('#enrolled-employee-id').val();
// 	var state = $(e.currentTarget).val();
// 	var courseId = $(e.currentTarget).attr('id');
// 	var course = $(e.currentTarget).parent().prev().text();

// 	$.get('/state/'+empId+'?id='+courseId+'&state='+state+'&course='+course, function(data, status) {
// 		console.log('MSG: ',data);
// 	   $('.state-alert').show();
// 	   $('.state-alert .state-message').text(data.message);
// 	   $('.state-alert').fadeOut(6000, function(){ 
// 		    $(this).hide();
// 		});
// 	});
// });

// Load existing data onto edit modal window
$('.edit-info').click(function(e){
	var empId = $(e.currentTarget).parent().parent().find('.hrid').text();
    $.get("/employees/"+empId, function(data, status){
        $('#fullname').val(data[0]['name']);
        $('#contact-no').val(data[0]['contact_no']);
        $('#edit-department').val(data[0]['department']);
        $('#designation').val(data[0]['designation']);
        $('body').data('empid', empId);
        $('input[name="empId"]').attr('value', empId);
        $('#edit-form').attr('action', 'employees/'+empId);
        $.each(data, function(id, obj) {
			$('#course_'+obj.course_id).val(obj.status);
			$('#start_date_'+obj.course_id).val(obj.start_date);
			$('#end_date_'+obj.course_id).val(obj.end_date);
        });
    });
});

if (departmentName != null) $('#search-department').val(departmentName);
if (moduleName != null) $('#search-course').val(moduleName);
if (moduleState != null) $('#search-course-state').val(moduleState);
if (year != null) $('#search-years').val(year);
// if (batch != null) $('#search-batches').val(batch);

$('.enroll-employee').click(function (e) {
	var empId = $(e.currentTarget).parent().parent().find('.hrid').text();
    $('input[name="empId"]').attr('value', empId);
    $('#enroll-form').attr('action', 'enroll/'+empId);
});

// Get employee's HRID for upload modal
$('.upload-file').click(function(e) {
	var empId = $(e.currentTarget).parent().parent().find('.hrid').text();
	$('#employee_id').val(empId);
});

// Display uploaded files in modal
$('.view-file').click(function(e) {
	$('.temp-row').remove();
	var empId = $(e.currentTarget).parent().parent().find('.hrid').text();
    $.get("/files/"+empId, function(data, status){
    	if (data[0]['course_name'] == null) {
    		$('#files-table > tbody:last-child').append("<tr class='temp-row'><td>"+'No results to show'+"</td></tr>");
    	} else {
			$.each(data, function(key, value) {
				var hrefCertificate = 'Not uploaded';
				var hrefTestPaper = 'Not uploaded';
				if (value.course_result != null) {
					hrefTestPaper = "<a href="+value.course_result+">View Test Paper</a>";
				}
				if (value.certificate != null) {
					hrefCertificate = "<a href="+value.certificate+">View Agreement</a>";
				}
				var tableRow = "<tr class='temp-row'><td>"+value.course_name+"</td><td>"+
					hrefTestPaper+"</td><td>"+hrefCertificate+"</td></tr>";
				$('#files-table > tbody:last-child').append(tableRow);
			});
    	}
    });
});

// Display enrolled courses in modal
$('.employee-courses').click(function(e) {
	$('.temp-row').remove();
	$('.course-details-name').text('');
	var empId = $(e.currentTarget).parent().parent().find('.hrid').text();
    $.get("/courses/"+empId, function(data, status){
    	if (data[0]['course_name'] == null) {
    		$('#courses-table > tbody:last-child').append("<tr class='temp-row'><td>"+'No enrolled courses to show'+"</td></tr>");
    	} else {
    		$('.course-details-name').text('Name: '+data.name+' ( '+data.empId+' )'+' Department: '+data.department);
    		$('#enrolled-employee-id').val(data.empId);
    		delete data['name'];
    		delete data['empId'];
    		delete data['department'];
    		var num = 0;
			$.each(data, function(key, value) {
				console.log('value => ',value);
				num++;
				var dropDwnId = value.id;
				var dropDwn = "<select disabled class='sm-inpt dynamic-course-state' name='state_"+dropDwnId+"' id='"+dropDwnId+"''><option value='not-specified'>None</option><option value='ongoing'>Ongoing</option><option value='pass'>Passed</option><option value='fail'>Failed</option><option value='absent'>Absent</option><option value='dnp'>DNP</option></select>";
				var tableRow = "<tr class='temp-row' id='rcd-"+value.id+"'><td><input type='radio' name='selected-course' value='"+value.id+"'> "+value.course_name+"</td><td>"+
					dropDwn+"</td><td> <input type='text' class='sm-inpt' disabled name='start_date_"+value.id+"' value='"+value.start_date+"'></td><td><input type='text' class='sm-inpt' disabled name='end_date_"+value.id+"' value='"+value.end_date+"'></td><td><input type='text' class='sm-inpt' disabled name='attendance_"+value.id+"' value='"+value.attendance+"'></td><td><input type='text' class='sm-inpt' disabled name='marks_"+value.id+"' value='"+value.marks+"'></td><td><input type='text' class='sm-inpt' disabled name='batch_"+value.id+"' value='"+value.batch+"'></td><td>"+"<button type='button' class='btn btn-default btn-sm edit-course-item'>Edit</button>"+"</td></tr>";
				$('#courses-table > tbody:last-child').append(tableRow);
				$('#'+dropDwnId).val(value.status);
			});
    	}
    });
});

$(document).on('click', '.edit-course-item', function(e) {
	var trId = $(e.currentTarget).parent().parent().attr('id');
	$('#'+trId+' .sm-inpt').prop('disabled', false);
	$('#'+trId+' .dynamic-course-state').prop('disabled', false);
});

$(document).on('click', '.save-changes', function(e) {
	var arr = [];
	$('.temp-row').each(function(key, val) {
		var tdSecond = $('#'+$(val).attr('id')).find('td:nth-child(2)');
		var changes = {};
		if ($(tdSecond).prop('disabled')) {
			return true;
		} else {
			changes['course_record_id'] = $(val).attr('id').substring(4);
		}
		var trId = $(val).attr('id');
		$('#'+trId+' .sm-inpt').each(function(key,val){
			if (!$(val).prop('disabled')) {
				changes[$(val).attr('name').slice(0,-2)] = $(val).val();
			}
		});
		arr.push(changes);
	});
	console.log('changes JSON : ',arr);
	var url = '/course';
    $.ajax({
		type: "POST",
		url: url,
		data: {data : JSON.stringify(arr)},
		dataType: "json",
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		success: function(data)
		{
		   $('.upload-alert').show();
		   $('.upload-alert .upload-message').text(data.message);
		   $('.upload-alert').fadeOut(5000, function(){ 
			    $(this).hide();
			});
		}
    });
});

$('.upload-alert').hide();
$('.state-alert').hide();

// Upload files to server (i.e. test papers and/or certificates)
$("form#file-upload-form").submit(function(e) {
	console.log("UPLOADING ...");
    $('#course_record_id').val($("input[name='selected-course']:checked").val());

    var url = "/upload"; // the script where you handle the form input.(laravel route)

	var formData = new FormData($(this)[0]);

    $.ajax({
		type: "POST",
		url: url,
		data: formData,
		async: false,
		success: function(data)
		{
		   $('.upload-alert').show();
		   $('.upload-alert .upload-message').text(data.message);
		   $('.upload-alert').fadeOut(5000, function(){ 
			    $(this).hide();
			});
		},
		cache: false,
        contentType: false,
    	processData: false
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});

$("form#edit-form").submit(function(e) {

	var emp_id = $('body').data();
	var url = "/employees/"+emp_id['empid'];
	var formData = new FormData($(this)[0]);
	console.log("form#edit-form");
    $.ajax({
		type: "POST",
		url: url,
		data: formData,
		async: false,
		success: function(data)
		{
		    $('#editModal').modal('hide');
			location.reload();
			$('.alert').fadeOut(5000, function(){ 
			    $(this).hide();
			});
		},
		cache: false,
        contentType: false,
    	processData: false
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});
