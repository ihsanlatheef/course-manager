<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{

	protected $employees = [
		['1091','M3369','Afa Ismail',	'HRD',	'Assistant Administrative Officer',	'7785855',	'Incomplete'],
		['1092','M3378','Mariyam Shausan',	'HRD',	'Assistant Administrative Officer',	'7798585',	'Incomplete'],
		['1093','M3516','Aishath Huyaam',	'Civil', 	'Assistant Administrative Officer',	'7948311',	'Incomplete'],
		['1094','M2946','Aminath Zuhudha Rafeeq', 'Civil', 'Assistant Public Relations Officer','7948312', 'Completed'],
		['1095','M3128','Hussain Areef', 'Civil', 	'Assistant HR Officer',	'7948313','Completed'],
		['1096','M1080','Fathmath Badhoora', 'HRD',	'Manager',	'7948314', 'Incomplete'],
		['1097','M1117','Fathmath Ali', 'HRD',	'Chief Manager',	'7948315', 'Incomplete'],
		['1098','M1573','Aminath Saaadha', 'HRD',	'Senior Manager',	'7948316', 'Incomplete'],
		['1099','M1881','Azumeen Abdul Kareem', 'HRD',	'Programmer',	'7948317', 'Incomplete'],
		['1100','M1829','Aminath Mohamed', 'ITD',	'Assistant Public Relations Officer',	'7948318', 'Incomplete'],
		['1101','M2104','Aishath Nadha', 'ITD',	'Human Resource Officer',	'7948319',	'Incomplete'],
		['1102','M2358','Fathimath  Lahfa', 'ITD',	'Human Resource Officer',	'7948320','Incomplete'],
		['1103','M2360','Aishath Areen Ilyas', 'ITD',	'Human Resource Officer',	'7948321',	'Incomplete'],
		['1104','M2465','Soodha Abdulla', 'ITD',	'Assistant Public Relations Officer',	'9442322',	'Incomplete'],
		['1105','M2795','Fathimath Niusha', 'ITD',	'Assistant Public Relations Officer',	'9442323',	'Incomplete'],
		['1106','M2933','Aminath Shidna', 'HRD',	'Assistant Administrative Officer',	'9442324',	'Incomplete'],
		['1107','M3160','Aishath Ziruvaa Zareer', 'HRD',	'Public Relations Officer',	'9442325',	'Incomplete'],
		['1108','M3275','Fathmath Faiha Farhad', 'HRD',	'Assistant Administrative Officer',	'9442326',	'Incomplete'],
		['1109','M3527','Iqbal Hameed', 'HRD',	'Manager',	'9442327',	'Incomplete'],
		['1110','M3688','Ismail Mahfooz Hashim', 'HRD',	'Assistant Administrative Officer',	'9442328',	'Incomplete'],
		['1111','M3695','Ahsan Abdul Rahman', 'Finance',	'Assistant Administrative Officer',	'9442329',	'Incomplete'],
		['1112','M3723','Mohamed Fikree', 'Finance',	'Assistant Engineer',	'9442330',	'Incomplete'],
		['1113','M3733','Saneeza', 'Finance',	'Assistant Administrative Officer',	'9442331',	'Incomplete'],
		['1114','M3759','Ismail Shareef', 'Finance',	'Assistant Administrative Officer',	'9442332', 'Incomplete'],
		['1115','M3760','Evana Jannath Binthi Azim', 'Finance',	'Assistant Public Relations Officer', '9442333', 'Incomplete'],
		['1116','M3761','Raaisa Mohamed',  'Finance',	'Assistant Public Relations Officer',	'9442334', 'Incomplete'],
		['1117','M3762','Fathimath Siyana',  'Finance',	'Assistant Public Relations Officer',	'9442335',	'Incomplete']
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach ($this->employees as $employee) {
	        DB::table('employee') -> insert([
	            'emp_hrid' => $employee[0],
	            'emp_id' => $employee[1],
	            'name' => $employee[2],
	            'department' => $employee[3],
	            'designation' => $employee[4],
	            'contact_no' => $employee[5],
	            'status' => $employee[6]
	        ]);
    	}
    }
}
