<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Http\Request as UploadRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show all employee records and display each employee's courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $courseRecords = DB::table('employee')->paginate(8);
        $employees = $courseRecords->items();
        $count = DB::table('employee')->get();

        $employeeArr = [];

        foreach ($employees as $employee) {
            $query = DB::table('course_employee')->where('emp_id', $employee->emp_id)
                ->get()->map(function ($item, $key) {
                    return (array) $item;
                })->all();
            $temp = [];
            $temp['emp_hrid'] = $employee->emp_hrid;
            $temp['name'] = $employee->name;
            $temp['department'] = $employee->department;
            $temp['overall_status'] = $employee->overall_status;
            $temp['year'] = isset($employee->year) ? $employee->year : 'all-years';

            array_push($employeeArr, $temp);
        }

        // Convert the array to an eloquent collection
        collect($employeeArr);

        // Prepare the collection for pagination results
        $perPage = 8;
        $currentPage = Input::get('page') ?: 1;
        $deliveries = new LengthAwarePaginator($employeeArr, count($count), $perPage, $currentPage);
        $deliveries ->setPath('/home');

        return view('employees', ['records' => $deliveries, 'searchparams' => ['department' => 'All', 'course' => 'all-courses', 'state' => 'all-states', 'year' => 'all-years', 'batch' => 'all-batches']]);
    }


    /**
     * Search employees by courses/department/course state/name/year.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $department = \Request::all()['search-department'];
        $course = \Request::all()['search-course'];
        $courseState = \Request::all()['search-course-state'];
        $year = \Request::all()['search-years'];
        $batch = \Request::all()['search-batches'];
        $param = \Request::all()['txt-search'];

        if ($course == 'all-courses') {
            $courseParams = ['1', '2', '3'];
        } else {
            $courseParams = [$course];
        }

        if ($courseState == 'all-states') {
            $courseStateParams = ['pass', 'fail', 'dnp', 'absent', 'ongoing'];
        } else {
            $courseStateParams = [$courseState];
        }

        if ($department == 'All') {
            $departmentParams = ["MACD","TVU","MEED","FID","Civil Works","SSD","COD","ADMIN and LEGAL","CCD","MAED","EED","Port Complex","HRD","THILAFUSHI","PRD","ITD","MNH","Harbour Master's Off","PTC","PILOTAGE and MSD","CEO Bureau","Operations Division","IAD","Engineering Division","T-Jetty","Southern Transport L","Cooperative Society","Strategic Planning a","Legal Unit","Corporate Services D","Media Unit"];
        } else {
            $departmentParams = [$department];
        }

        if ($year == 'all-years') {
            $yearParams = ['2015', '2016', '2017', '2018'];
        } else {
            $yearParams = [$year];
        }

        if ($batch == '') {
            $batchParams = '20%';
        } else {
            $batchParams = '%'.$batch.'%';
        }

        $searchResults = DB::table('employee')
                        ->join('course_employee', 'employee.emp_id', '=', 'course_employee.emp_id')
                        ->whereIn('course_employee.course_id', $courseParams)
                        ->whereIn('course_employee.status', $courseStateParams)
                        ->whereIn('employee.department', $departmentParams)
                        ->whereIn('course_employee.year', $yearParams)
                        ->where('course_employee.batch', 'like', $batchParams)
                        ->distinct()
                        ->get(['employee.emp_hrid', 'employee.emp_id', 'employee.name', 'employee.department', 'course_employee.marks','course_employee.status', 'course_employee.course_id', 'course_employee.batch','employee.overall_status', 'course_employee.year','course_employee.attended_classes','course_employee.total_classes']);

        $searchParams = [];
        $searchParams['department'] = (isset($department) ? $department : null);
        $searchParams['course'] = (isset($course) ? $course : null);
        $searchParams['state'] = (isset($courseState) ? $courseState : null);
        $searchParams['year'] = (isset($year) ? $year : null);
        $searchParams['batch'] = (isset($batch) ? $batch : null);

        if (isset($param)) {
            $searchResults = DB::table('employee')
            ->where('employee.name', 'like', '%'.$param.'%')
            ->orWhere('employee.emp_id', 'like', '%'.$param.'%')
            ->get(['employee.emp_hrid', 'employee.emp_id', 'employee.name', 'employee.department', 'employee.overall_status']);
            $employeeArr = [];
            $courseName = '-';
            foreach ($searchResults as $employee) {
                $temp = [];
                $temp['emp_hrid'] = $employee->emp_hrid;
                $temp['emp_id'] = $employee->emp_id;
                $temp['name'] = $employee->name;
                $temp['department'] = $employee->department;
                $temp['overall_status'] = $employee->overall_status;
                array_push($employeeArr, $temp);
            } 
            $count = sizeof($employeeArr);
            collect($employeeArr);

            // Prepare the collection for pagination results
            $perPage = 1000000;
            $currentPage = Input::get('page') ?: 1;
            $deliveries = new LengthAwarePaginator($employeeArr, $count, $perPage, $currentPage);
            $deliveries ->setPath('/home');

            return view('employees', ['records' => $deliveries, 'searchparams' => $searchParams]);
        } else {
            $employeeArr = [];
            $courseName = '-';
            foreach ($searchResults as $employee) {
                $temp = [];
                $temp['emp_hrid'] = $employee->emp_hrid;
                $temp['emp_id'] = $employee->emp_id;
                $temp['name'] = $employee->name;
                $temp['department'] = $employee->department;
                $temp['overall_status'] = $employee->overall_status;
                $temp['marks'] = $employee->marks;
                $temp['batch'] = $employee->batch;
                $temp['status'] = $employee->status;
                $temp['year'] = $employee->year;
                $temp['attendance'] = isset($employee->total_classes) && isset($employee->attended_classes) && $employee->total_classes != 0 ? intVal(($employee->attended_classes / $employee->total_classes) * 100) : 0;
                if ($employee->course_id == '1') {
                    $courseName = 'C 1.1';
                } elseif ($employee->course_id == '2') {
                    $courseName = 'C 3.1';
                } else {
                    $courseName = 'C 4.1';
                }
                $temp['course_id'] = $courseName;

                array_push($employeeArr, $temp);
            } 
        }

        $count = sizeof($employeeArr);
        collect($employeeArr);

        // Prepare the collection for pagination results
        $perPage = 1000000;
        $currentPage = Input::get('page') ?: 1;
        $deliveries = new LengthAwarePaginator($employeeArr, $count, $perPage, $currentPage);
        $deliveries ->setPath('/home');

        return view('employees', ['searchResults' => $deliveries, 'searchparams' => $searchParams]);
    }


    /**
     * Save a new employee's info along with course details.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveEmployee()
    {
        $params = Request::all();
        
        try {
            $insertedEmployee = DB::table('employee')->insert([
                    [
                        'emp_id' => $params['employee_id'], 'emp_hrid' => $params['employee_hrid'],
                        'name' => $params['fullname'], 'contact_no' => $params['contact_no'], 
                        'department' => $params['department'], 'designation' => $params['designation'],
                        'overall_status' => 'Incomplete', 'unit' => $params['employee_unit']
                    ]
                ]);
            $insertedTransaction = DB::table('transaction')->insert([
                    ['user' => Auth::user()->name, 'type' => 'Created New Employee ('.$params['employee_id'].')']
                ]);

        } catch (\Exception $e) {
            \Session::flash('error', 'Failed creating new employee record!'); 
            return redirect()->route('home')->withStatus('error', 'Failed creating new employee record!'); 
        }

        if ($insertedEmployee)
        {
            \Session::flash('message', 'Successfully added new employee!'); 
            return redirect()->route('home')->withStatus('message', 'Successfully added new employee!');            
        }
        else
        {
            \Session::flash('error', 'Failed creating new employee record!'); 
            return redirect()->route('home')->withStatus('error', 'Failed creating new employee record!'); 
        }
    }


    /**
     * Get a single employee's details.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmployee()
    {
        $id = \Request::route('id');
        $employee = DB::table('employee')
            ->leftJoin('course_employee', 'course_employee.emp_id', '=', 'employee.emp_id')
            ->leftJoin('course', 'course.course_id', '=', 'course_employee.course_id')
            ->where('emp_hrid', $id)->get();

        return \Response::json($employee);
    }


    /**
     * Update a single employee's details.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateEmployee()
    {
        $params = Request::all();

        try {
            $empId = DB::table('employee')->where('emp_hrid', '=', $params['empId'])->pluck('emp_id');

            DB::table('employee')
                ->where('emp_id', '=', $empId)
                ->update(['name' => $params['fullname'], 'contact_no' => $params['contactno'], 'department' => $params['edit-department'], 'designation' => $params['designation']]);
            
            $insertedTransaction = DB::table('transaction')->insert([
                    ['user' => Auth::user()->name, 'type' => 'Updated Employee Details '.$empId]
                ]);

        } catch (\Exception $e) {
            \Session::flash('message', 'Sorry! Employee details could not be updated due to an error!'); 
            return \Response::json(['message' => 'Sorry! Employee details could not be updated due to an error!']);
        }

        \Session::flash('message', 'Successfully updated employee details!'); 
        return response()->json(['message' => 'Successfully updated employee details!']); 
    }

    /**
     * Update an employee's course/module state.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCourseState()
    {
        $params = Request::all();
        $empId = \Request::route('empId');
        $id = $params['id'];
        $state = $params['state'];

        try {
            $updatedCourse = DB::table('course_employee')
                ->where([
                    ['emp_id', '=', $empId],
                    ['id', '=', $id]])
                ->update(['status' => $state]);
            $userFullName = Auth::user()->name;
            $insertedTransaction = DB::table('transaction')->insert([
                    ['user' => $userFullName, 'type' => 'Updated Employee ('.$empId.') '.$params['course'].' to '.$state]
                ]);

        } catch (\Exception $e) {
            // \Session::flash('message', 'Sorry! course details could not be updated due to an error!'); 
            return \Response::json(['message' => 'Sorry! Course details could not be updated due to an error!']);
        }

        // \Session::flash('message', 'Successfully updated course details!'); 
        return response()->json(['message' => 'Successfully updated ('.$params['course'].') module state to '.$state.'!']); 
    }

    /**
     * Upload certificate/test result of employee to server.
     *
     * @param  Request  $request
     * @return Response
     */
    public function upload(UploadRequest $request)
    {
        $params = $request->all();

        $records = DB::table('course_employee')
                    ->where('id', $params['course_record_id'])->get();

        $allowedMimeTypes = ['image/bmp', 'image/jpeg', 'application/pdf', 'image/png'];

        if (!in_array($request->file('result-file')->getMimeType(), $allowedMimeTypes)) 
        {
            return \Response::json(['message' => 'File type not allowed! Only JPEG, BMP, PNG and PDF allowed.']);
        }
        
        if ($records->count() == 0)
        {
            return \Response::json(['message' => 'Please select a course before uploading files!']);
        }

        if ($params['file-type'] == 'test-result') 
        {
            if ($request->file('result-file')->isValid()) 
            {
                $filename = $request->file('result-file')->store('test-results');
                $updated = DB::table('course_employee')->where('id', $params['course_record_id'])
                            ->update(['testpaper' => $filename]);
                $insertedTransaction = DB::table('transaction')->insert([
                    ['user' => Auth::user()->name, 'type' => 'Uploaded test result for employee ('.$records[0]->emp_id.')']
                ]);
                if ($updated) {
                    return \Response::json(['message' => 'Successfully uploaded test paper!']);
                } else {
                    return \Response::json(['message' => 'Could not upload test paper!']);
                }
            }
        }
        elseif ($params['file-type'] == 'certificate') 
        {
            if ($request->file('result-file')->isValid()) 
            {
                $filename = $request->file('result-file')->store('certificates');
                $updated = DB::table('course_employee')->where('id', $params['course_record_id'])
                            ->update(['certificate' => $filename]);
                $insertedTransaction = DB::table('transaction')->insert([
                    ['user' => Auth::user()->name, 'type' => 'Uploaded agreement for employee ('.$records[0]->emp_id.')']
                ]);

                if ($updated) {
                    return \Response::json(['message' => 'Successfully uploaded agreement!']);
                } else {
                    return \Response::json(['message' => 'Could not upload agreement!']);
                }
            }

        } 
        else 
        {
            return \Response::json(['message' => 'File category coud not be identified! Should be either agreement or test result']);
        }
        
    }

    /**
     * Get enrolled courses for an employee.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getCourses()
    {
        $id = \Request::route('employeeId');

        $results = DB::table('employee')
            ->leftJoin('course_employee', 'course_employee.emp_id', '=', 'employee.emp_id')
            ->leftJoin('course', 'course.course_id', '=', 'course_employee.course_id')
            ->where('emp_hrid', $id)->get();

        $empArr = [];
        $empArr['empId'] = $results[0]->emp_id;
        $empArr['name'] = $results[0]->name;
        $empArr['department'] = $results[0]->department;

        foreach ($results as $result) {
            $arr = [];
            $arr['id'] = $result->id;
            $arr['course_name'] = $result->course_name;
            $arr['status'] = $result->status;
            $arr['start_date'] = (isset($result->start_date) ? $result->start_date : 'NA');
            $arr['end_date'] = (isset($result->end_date) ? $result->end_date : 'NA');
            $arr['batch'] = $result->batch;
            $arr['marks'] = $result->marks;
            $arr['attendance'] = isset($result->total_classes) && $result->total_classes != 0 ? intVal(($result->attended_classes / $result->total_classes) * 100) : 0;
            array_push($empArr, $arr);
        }

        return \Response::json($empArr);
    }


    /**
     * Get uploaded files (test results and certificates) for an employee.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getFiles()
    {
        $id = \Request::route('id');

        $results = DB::table('employee')
            ->leftJoin('course_employee', 'course_employee.emp_id', '=', 'employee.emp_id')
            ->leftJoin('course', 'course.course_id', '=', 'course_employee.course_id')
            ->where('emp_hrid', $id)->get();

        $empArr = [];

        foreach ($results as $result) {
            $arr = [];
            $arr['course_name'] = $result->course_name;
            if ($result->testpaper) {
                $arr['course_result'] = \Storage::url($result->testpaper);
            } else {
                $arr['course_result'] = null;
            }
            if ($result->certificate) {
                $arr['certificate'] = \Storage::url($result->certificate);
            } else {
                $arr['certificate'] = null;
            }
            array_push($empArr, $arr);
        }

        return \Response::json($empArr);
    }

    /**
     * Update course record details.
     *
     * @param  Request  $request
     * @return Response
     */
    public function updateCourseRecord()
    {
        $post = json_decode(\Request::all()['data']);
        foreach ($post as $item) {
            if (array_key_exists('state', $item)) {
                $updatedCourse = DB::table('course_employee')
                    ->where('id', '=', $item->course_record_id)
                    ->update(['status' => $item->state, 'attended_classes' => $item->attendance, 'start_date' => $item->start_date, 'end_date' => $item->end_date, 'marks' => $item->marks, 'batch' => $item->batch]);
                if ($updatedCourse) {
                    $insertedTransaction = DB::table('transaction')->insert([
                        ['user' => Auth::user()->name, 'type' => 'Updated course record with id '.$item->course_record_id]
                    ]);
                } else {
                    return response()->json(['error' => 'Failed updating course details due to an error.']); 
                }
            }
        }

        return response()->json(['message' => 'Successfully updated course details.']); 
    }

    /**
     * Enroll an employee in a course.
     *
     * @return \Illuminate\Http\Response
     */
    public function enrollEmployee()
    {
        $params = Request::all();

        $empId = DB::table('employee')->where('emp_hrid', '=', $params['empId'])->pluck('emp_id');

        switch ($params['course_name']) {
            case '1':
                $txtValue = 'C 1.1';
                break;
            case '2':
                $txtValue = 'C 3.1';
                break;
            case '3':
                $txtValue = 'C 4.1';
                break;
        }

        $courseRecordC1 = DB::table('course_employee')
            ->where('course_id', '=', '1')
            ->where('emp_id', '=', $empId)->get();

        $courseRecordC3 = DB::table('course_employee')
            ->where('course_id', '=', '2')
            ->where('emp_id', '=', $empId)->get();

        $courseRecordC4 = DB::table('course_employee')
            ->where('course_id', '=', '3')
            ->where('emp_id', '=', $empId)->get();

        $passedC1 = false;
        $passedC2 = false;

        if (!$courseRecordC1->isEmpty()) {
            foreach ($courseRecordC1 as $item) {
                if ($item->status == 'pass') $passedC1 = true;
                if ($item->status == 'pass' &&  $params['course_name'] == '1') {
                    \Session::flash('error', 'This employee has already passed in C 1.1'); 
                    return redirect()->route('home')->withStatus('error', 'This employee has already passed in C 1.1');
                }
            }
        }

        if (!$passedC1 && $params['course_name'] == '2') {
            \Session::flash('error', 'C 1.1 should be completed prior to enrollment in C 3.1');
            return redirect()->route('home')->withStatus('error', 'C 1.1 should be completed prior to enrollment in C 3.1');
        }

        if (!$courseRecordC3->isEmpty()) {
            $invalidEnrollment = true;
            foreach ($courseRecordC3 as $item) {
                if ($item->status == 'pass') $passedC2 = true;
                if ($item->status == 'pass' &&  $params['course_name'] == '2') {
                    \Session::flash('error', 'This employee has already passed in C 3.1'); 
                    return redirect()->route('home')->withStatus('error', 'This employee has already passed in C 3.1');
                }
            }
        }

        if ((!$passedC1 && !$passedC2 && $params['course_name'] == '3') 
                || (!$passedC1 && $passedC2 && $params['course_name'] == '3')
                    || ($passedC1 && !$passedC2 && $params['course_name'] == '3')) {
            \Session::flash('error', 'C 3.1 should be completed prior to enrollment in C 4.1');
            return redirect()->route('home')->withStatus('error', 'C 3.1 should be completed prior to enrollment in C 4.1');
        }

        if (!$courseRecordC4->isEmpty()) {
            foreach ($courseRecordC4 as $item) {
                if ($item->status == 'pass' &&  $params['course_name'] == '3') {
                    \Session::flash('error', 'This employee has already passed in C 3.1'); 
                    return redirect()->route('home')->withStatus('error', 'This employee has already passed in C 3.1');
                }
            }
        }

        if ($passedC1 && $passedC2 && $params['course_name'] == '3' && $params['course_state'] == 'pass') {
            DB::table('employee')
                ->where('emp_id', '=', $empId[0])
                ->update(['overall_status' => 'completed']);
        }

        DB::table('course_employee')
            ->insert(['emp_id' => $empId[0], 'course_id' => $params['course_name'], 
                    'status' => $params['course_state'], 
                    'start_date' => $params['course_start_date'],
                    'end_date' => $params['course_end_date'],
                    'batch' => $params['batch_no'],
                    'year' => $params['year'],
                    'marks' => $params['marks'],
                    'attended_classes' => $params['attended_classes'],
                    'total_classes' => $params['total_classes']]);

        $insertedTransaction = DB::table('transaction')->insert([
                    ['user' => Auth::user()->name, 'type' => 'Enrolled employee ('.$params['empId'].') in '.$txtValue]
                ]);

        \Session::flash('message', 'Successfully enrolled employee in course!'); 
        return redirect()->route('home')->withStatus('message', 'Successfully enrolled employee in course!'); 
    }

    /**
     * Get actions performed by employees (transactions).
     *
     * @return \Illuminate\Http\Response
     */
    public function getTransactionHistory()
    {
        $transactionHistory = DB::table('transaction')->paginate(8);
        $transactions = $transactionHistory->items();
        $count = DB::table('transaction')->get();

        $transactionArr = [];

        foreach ($transactions as $transaction) {
            $temp = [];
            $temp['name'] = $transaction->user;
            $temp['action'] = $transaction->type;
            $temp['date'] = $transaction->date;

            array_push($transactionArr, $temp);
        }

        // Convert the array to an eloquent collection
        collect($transactionArr);

        // Prepare the collection for pagination results
        $perPage = 8;
        $currentPage = Input::get('page') ?: 1;
        $deliveries = new LengthAwarePaginator($transactionArr, count($count), $perPage, $currentPage);
        $deliveries ->setPath('/transactions');

        return view('transactions', ['records' => $deliveries]);
    }
}
