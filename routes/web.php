<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/add', function() {
	return view('add');
});

Route::get('/employees', 'HomeController@getEmployees')->name('employees');

Route::get('/employees/{id}', 'HomeController@getEmployee');

Route::post('/employees/{id}', 'HomeController@updateEmployee');

Route::post('/enroll/{id}', 'HomeController@enrollEmployee');

Route::post('/search', 'HomeController@search');

Route::post('/upload', 'HomeController@upload');

Route::post('/employees', 'HomeController@saveEmployee');

Route::get('/files/{id}', 'HomeController@getFiles');

Route::get('/courses/{employeeId}', 'HomeController@getCourses');

Route::post('/course', 'HomeController@updateCourseRecord');

Route::get('/state/{empId}', 'HomeController@updateCourseState');

Route::get('/transactions', 'HomeController@getTransactionHistory');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');