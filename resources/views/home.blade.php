@extends('layouts.app')

@section('content')
<div class="container">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
              <div class="container">
                <div class="row">
                  <div class="col-lg-4 col-md-offset-8">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search for...">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Search</button>
                      </span>
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                </div>
                <div class="row">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Designation</th>
                        <th>Course</th>
                        <th>Test result</th>
                        <th>Certificate</th>
                        <th>Overall Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if(!empty($records) && $records->count())
                            @foreach ($records as $record)
                                <tr>
                                    <td>{{ $record->emp_hrid }}</td>
                                    <td>{{ $record->name }}</td>
                                    <td>{{ $record->department }}</td>
                                    <td>{{ $record->designation }}</td>
                                    @if(is_null($record->course_name))
                                      <td> - </td>
                                    @else
                                      <td>{{ $record->course_name }}</td>
                                    @endif
                                    @if(is_null($record->testpaper))
                                      <td> - </td>
                                    @else
                                      <td>{{ $record->testpaper }}</td>
                                    @endif
                                    @if(is_null($record->certificate))
                                      <td> - </td>
                                    @else
                                      <td>{{ $record->certificate }}</td>
                                    @endif
                                    <td>{{ $record->overall_status }}</td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="10">There are no results to show.</td>
                            </tr>
                        @endif
                    </tbody>
                  </table>
                  {{ $records->links() }}
                </div>
              </div>
            @else
                @include('auth.login')
            @endif
        </div>
    @endif
</div>
@endsection
