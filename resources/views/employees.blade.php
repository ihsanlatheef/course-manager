@extends('layouts.app')

@section('content')
<div class="container">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
              <div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <form action="/search" method="POST" role="search">
                      {{ csrf_field() }}
                        <script>
                          var departmentName = "{{ $searchparams['department'] }}";
                          var moduleName = "{{ $searchparams['course'] }}";
                          var moduleState = "{{ $searchparams['state'] }}";
                          var year = "{{ $searchparams['year'] }}";
                          var batch = "{{ $searchparams['batch'] }}";
                        </script>
                        <div class="row">
                          <div class="col-md-2 search-selector">
                            <select class="form-control search-department" id="search-department" name="search-department">
                              <option value="All">All Departments</option>
                              <option value="MACD">MACD</option>
                              <option value="TVU">TVU</option>
                              <option value="MEED">MEED</option>
                              <option value="FID">FID</option>
                              <option value="Civil Works">Civil Works</option>
                              <option value="SSD">SSD</option>
                              <option value="COD">COD</option>
                              <option value="ADMIN and LEGAL">ADMIN and LEGAL</option>
                              <option value="CCD">CCD</option>
                              <option value="MAED">MAED</option>
                              <option value="EED">EED</option>
                              <option value="Port Complex">Port Complex</option>
                              <option value="HRD">HRD</option>
                              <option value="THILAFUSHI">THILAFUSHI</option>
                              <option value="PRD">PRD</option>
                              <option value="ITD">ITD</option>
                              <option value="MNH">MNH</option>
                              <option value="Harbour Master's Off">Harbour Master's Off</option>
                              <option value="PTC">PTC</option>
                              <option value="PILOTAGE and MSD">PILOTAGE and MSD</option>
                              <option value="CEO Bureau">CEO Bureau</option>
                              <option value="Operations Division">Operations Division</option>
                              <option value="IAD">IAD</option>
                              <option value="Engineering Division">Engineering Division</option>
                              <option value="T-Jetty">T-Jetty</option>
                              <option value="Southern Transport L">Southern Transport L</option>
                              <option value="Cooperative Society">Cooperative Society</option>
                              <option value="Strategic Planning a">Strategic Planning a</option>
                              <option value="Legal Unit">Legal Unit</option>
                              <option value="Corporate Services D">Corporate Services D</option>
                              <option value="Media Unit">Media Unit</option>
                            </select>
                          </div>
                          <div class="col-md-2 search-selector">
                            <select class="form-control search-course" id="search-course" name="search-course">
                              <option value="all-courses">All Modules</option>
                              <option value="1">C 1.1</option>
                              <option value="2">C 3.1</option>
                              <option value="3">C 4.1</option>
                            </select>
                          </div>
                          <div class="col-md-one-half search-selector">
                            <select class="form-control search-years" id="search-years" name="search-years">
                              <option value="all-years">All Years</option>
                              <option value="2015">2015</option>
                              <option value="2016">2016</option>
                              <option value="2017">2017</option>
                              <option value="2018">2018</option>
                            </select>
                          </div>
                          <div class="col-md-one-half search-selector">
                            <select class="form-control search-course-state" id="search-course-state" name="search-course-state">
                              <option value="all-states">All States</option>
                              <option value="ongoing">Ongoing</option>
                              <option value="pass">Passed</option>
                              <option value="fail">Failed</option>
                              <option value="absent">Absent</option>
                              <option value="dnp">DNP</option>
                            </select>
                          </div>
                          <div class="col-md-one-half search-selector">
                            <input type="text" class="form-control" id="search-batches" name="search-batches" placeholder="Batch ID">
                          </div>
                          <div class="col-md-3 search-selector">
                              <input type="text" class="form-control" id="txt-search" name="txt-search" placeholder="Search by name, EmpID or leave blank">
                          </div>
                          <div class="col-md-1 search-selector">
                            <button class="btn btn-default" id="btn-search" type="submit">Search</button>
                          </div>
                        </div>
                    </form>
                  </div><!-- /.col-lg-6 -->
                </div>
                <div class="row">&nbsp;</div>
                
                <!-- Display success message -->
                @if (session('message'))
                  <div class="row">
                    <div class="col-md-6 alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session('message') }}</strong>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                @endif

                <!-- Display failure message -->
                @if (session('error'))
                  <div class="row">
                    <div class="col-md-6 alert alert-danger alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session('error') }}</strong>
                    </div>
                    <div class="col-md-6"></div>
                  </div>
                @endif

                <!-- Show DB results -->
                <div class="table-responsive">
                  <table class="table table-hover" id="search-results-table">
                    <thead>
                      @if (isset($records))
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Department</th>
                          <th>Overall Status</th>
                          <th>Actions</th>
                        </tr>
                      @else
                        <tr>
                          <th>#</th>
                          <th>EmpID</th>
                          <th>Name</th>
                          <th>Department</th>
                          <th>Marks</th>
                          <th>Module</th>
                          <th>Year</th>
                          <th>Batch</th>
                          <th>State</th>
                          <th>Attendance(%)</th>
                          <th>
                          Actions
                            <div class="col-md-1 to-excel">
                              <button class="btn btn-default" type="button" id="toExcel" style="float:left;">Download as Excel File</button>
                            </div> 
                          </th>
                        </tr>
                      @endif
                    </thead>
                    <tbody>
                    @if (isset($records))
                      {{is_null($records)}}
                          @if(!empty($records))
                              @foreach ($records as $record)
                                  <tr>
                                      <td class="hrid">{{ $record['emp_hrid'] }}</td>
                                      <td>{{ $record['name'] }}</td>
                                      <td>{{ $record['department'] }}</td>
                                      <td>{{ $record['overall_status'] }}</td>
                                      <td>
                                        <button class="btn btn-primary btn-xs employee-courses" type="button"
                                         data-toggle="modal" data-target="#coursesModal">View Modules</button>
                                        <button class="btn btn-primary btn-xs enroll-employee" type="button"
                                         data-toggle="modal" data-target="#enrollModal">Enroll</button>
                                        <button class="btn btn-primary btn-xs edit-info" type="button"
                                         data-toggle="modal" data-target="#editModal">Edit info</button>
                          <!--               <button class="btn btn-primary btn-xs upload-file" type="button"
                                         data-toggle="modal" data-target="#uploadModal">Upload file</button> -->
                                        <button class="btn btn-primary btn-xs view-file" type="button"
                                         data-toggle="modal" data-target="#fileViewModal">View files</button>
                                      </td>
                                  </tr>
                              @endforeach
                          @unless(count($records)) 
                              <tr>
                                  <td colspan="10">No course records match the specified criteria.</td>
                              </tr>
                          @endunless
                          @endif
                    @else
                      {{is_null($searchResults)}}
                          @if(!empty($searchResults))
                              @foreach ($searchResults as $result)
                                  <tr>
                                      <td class="hrid">{{ $result['emp_hrid'] }}</td>
                                      <td>{{ $result['emp_id'] }}</td>
                                      <td>{{ $result['name'] }}</td>
                                      <td>{{ $result['department'] }}</td>
                                      <td>{{ $result['marks'] }}</td>
                                      <td>{{ $result['course_id'] }}</td>
                                      <td>{{ $result['year'] }}</td>
                                      <td>{{ $result['batch'] }}</td>
                                      <td>{{ $result['status'] }}</td>
                                      <td>{{ $result['attendance'] }}</td>
                                      <td class="to-be-removed">
                                        <button class="btn btn-primary btn-xs employee-courses" type="button"
                                         data-toggle="modal" data-target="#coursesModal">View Modules</button>
                                        <button class="btn btn-primary btn-xs enroll-employee" type="button"
                                         data-toggle="modal" data-target="#enrollModal">Enroll</button>
                                        <button class="btn btn-primary btn-xs edit-info" type="button"
                                         data-toggle="modal" data-target="#editModal">Edit info</button>
       <!--                                  <button class="btn btn-primary btn-xs upload-file" type="button"
                                         data-toggle="modal" data-target="#uploadModal">Upload file</button> -->
                                        <button class="btn btn-primary btn-xs view-file" type="button"
                                         data-toggle="modal" data-target="#fileViewModal">View files</button>
                                      </td>
                                  </tr>
                              @endforeach
                          @unless(count($searchResults)) 
                              <tr>
                                  <td colspan="10">No course records match the specified criteria.</td>
                              </tr>
                          @endunless
                          @endif
                    @endif
                    </tbody>
                  </table>
                  @if (isset($records))
                    {{ $records->links() }}
                  @else
                    {{ $searchResults->links() }}
                  @endif
                </div>
              </div>
            @else
                @include('auth.login')
            @endif
        </div>
    @endif
</div>
@endsection

<!-- Modal for editing employee info-->
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Details</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="edit-form" action="/employees" method="post">
            {{ csrf_field() }}
            <div class="panel panel-info" data-empid="">
              <div class="panel-heading">
                <h3 class="panel-title">Personal Info</h3>
              </div>
              <div class="panel-body panel-margin-style">
                <input type="hidden" id="employee-id" name="empId" value=""/>
                <div class="row">
                  <div class="col-md-6" style="padding-right: 20px;">
                    <div class="form-group">
                        <label for="fullname" class="control-label">Full Name</label>
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="fullname">
                    </div>
                    <div class="form-group">
                        <label for="contact-no" class="control-label">Contact No</label>
                        <input type="text" class="form-control" id="contact-no" name="contactno" placeholder="contact no">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="test-result" class="control-label">Department</label>
                        <select class="form-control edit-department" id="edit-department" name="edit-department">
                              <option value="All">All Departments</option>
                              <option value="MACD">MACD</option>
                              <option value="TVU">TVU</option>
                              <option value="MEED">MEED</option>
                              <option value="FID">FID</option>
                              <option value="Civil Works">Civil Works</option>
                              <option value="SSD">SSD</option>
                              <option value="COD">COD</option>
                              <option value="ADMIN and LEGAL">ADMIN and LEGAL</option>
                              <option value="CCD">CCD</option>
                              <option value="MAED">MAED</option>
                              <option value="EED">EED</option>
                              <option value="Port Complex">Port Complex</option>
                              <option value="HRD">HRD</option>
                              <option value="THILAFUSHI">THILAFUSHI</option>
                              <option value="PRD">PRD</option>
                              <option value="ITD">ITD</option>
                              <option value="MNH">MNH</option>
                              <option value="Harbour Master's Off">Harbour Master's Off</option>
                              <option value="PTC">PTC</option>
                              <option value="PILOTAGE and MSD">PILOTAGE and MSD</option>
                              <option value="CEO Bureau">CEO Bureau</option>
                              <option value="Operations Division">Operations Division</option>
                              <option value="IAD">IAD</option>
                              <option value="Engineering Division">Engineering Division</option>
                              <option value="T-Jetty">T-Jetty</option>
                              <option value="Southern Transport L">Southern Transport L</option>
                              <option value="Cooperative Society">Cooperative Society</option>
                              <option value="Strategic Planning a">Strategic Planning a</option>
                              <option value="Legal Unit">Legal Unit</option>
                              <option value="Corporate Services D">Corporate Services D</option>
                              <option value="Media Unit">Media Unit</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="designation" class="control-label">Designation</label>
                        <input type="text" class="form-control" id="designation" name="designation" placeholder="contact no">
                    </div>
                  </div>
                </div>
              </div>
            </div> 
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary btn-update" />
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>

  </div>
</div>

<!-- Modal for uploading employee results/certificate-->
<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload File(s)</h4>
      </div>
      <div class="modal-body">
        <form action="/upload" method="post" id="file-upload-form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" id="employee_id" name="employee_id" value="">
            <div class="alert alert-info alert-dismissable upload-alert">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong class="upload-message"></strong>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="file-type" class="control-label">File Type</label><br />
                <input type="radio" name="file-type" value="certificate" checked> Agreement
                <input type="radio" name="file-type" value="test-result"> Test Paper <br /><br />
                <label for="test-result" class="control-label">Select Module</label>
                <select class="form-control test-result" name="course-id">
                  <option value="1">Module C 1.1</option>
                  <option value="2">Module C 3.1</option>
                  <option value="3">Module C 4.1</option>
                </select>
              </div>
              <div class="col-md-6">
                Select file (please choose one file):
                <br />
                <input type="file" name="result-file" multiple />
                <br />
                <input type="submit" class="btn btn-sm btn-primary" value="Upload" />
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal for viewing uploaded results/certificate-->
<div id="fileViewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Uploaded File(s)</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table" id="files-table">
            <thead>
              <tr>
                <th>Module</th>
                <th>Test Paper</th>
                <th>Agreement</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal for enrolling employees in courses-->
<div id="enrollModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" data-empid="">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enroll in Module(s)</h4>
      </div>
      <div class="modal-body">
        <form action="/enroll" method="post" id="enroll-form">
            {{ csrf_field() }}
          <input type="hidden" id="employee-id" name="empId" value=""/>
          <div class="row">
            <div class="col-md-4">
             <div class="form-group">
                 <label for="contact-no" class="control-label">Module</label>
                 <select name="course_name" class="form-control course-name" id="course-name">
                   <option value="1">C 1.1</option>
                   <option value="2">C 3.1</option>
                   <option value="3">C 4.1</option>
                 </select>
             </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                 <label for="contact-no" class="control-label">Status</label>
                 <select name="course_state" class="form-control course-state" id="course-state">
                   <option value="ongoing">Ongoing</option>
                   <option value="dnp">Did Not Participate</option>
                   <option value="pass">Passed</option>
                   <option value="fail">Failed</option>
                   <option value="absent">Absent</option>
                 </select>
              </div> 
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="batch-no" class="control-label">Batch No</label>
                <input type="text" class="form-control" id="batch-no" name="batch_no">
              </div>
            </div> 
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="marks" class="control-label">Marks</label>
                <input type="text" class="form-control" id="marks" name="marks">
              </div>
            </div>
            <div class="col-md-4">
             <div class="form-group">
                 <label for="start_date" class="control-label">Start Date</label>
                 <input type="date" name="course_start_date" class="form-control" id="start_date">
             </div>          
            </div>
            <div class="col-md-4">
              <div class="form-group">
               <label for="end_date" class="control-label">End Date</label>
               <input type="date" name="course_end_date" class="form-control" id="end_date">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="marks" class="control-label">Attended Classes</label>
                <input type="number" class="form-control" id="marks" name="attended_classes">
              </div>
            </div>
            <div class="col-md-4">
             <div class="form-group">
                 <label for="start_date" class="control-label">Total Classes</label>
                 <input type="number" name="total_classes" class="form-control" id="total_classes">
             </div>          
            </div>
            <div class="col-md-4">
              <div class="form-group">
               <label for="end_date" class="control-label">Year</label>
               <input type="text" name="year" class="form-control" id="year">
              </div>
            </div>
          </div>
      </div> <!-- modal-body -->
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary btn-enroll" />
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>

  </div>
</div>

<!-- Modal for viewing enrolled courses and changing course status-->
<div id="coursesModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" data-empid="">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enrolled Module(s) Details</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-info alert-dismissable state-alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong class="state-message"></strong>
        </div>
        <h5 class="course-details-name" style="padding-left: 6px; font-weight: bold;">Name: </h5>
        <div class="table-responsive">
          <input type="hidden" id="enrolled-employee-id" value=""/>
            <table class="table" id="courses-table">
              <thead>
                <tr>
                  <th>Module</th>
                  <th>Status</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Attendance (%)</th>
                  <th>Marks</th>
                  <th>Batch</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <hr>
          <div class="upload-form">
            <form action="/upload" method="post" id="file-upload-form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" id="course_record_id" name="course_record_id" value="">
                <div class="alert alert-info alert-dismissable upload-alert">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong class="upload-message"></strong>
                </div>
                <div class="row">
                  <div class="col-md-12"><p><strong>Select a course from the above table to upload a file.</strong></p></div>
                  <div class="col-md-6">
                    <label for="file-type" class="control-label">File Type</label><br />
                    <input type="radio" name="file-type" value="certificate" checked> Agreement
                    <input type="radio" name="file-type" value="test-result"> Test Paper <br /><br />
                  </div>
                  <div class="col-md-6">
                    Select file (please choose one file):
                    <br />
                    <input type="file" name="result-file" multiple />
                    <br />
                    <input type="submit" class="btn btn-sm btn-primary" value="Upload" />
                  </div>
                </div>
            </form>
          </div>
      </div> <!-- modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary save-changes">Save Changes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>

  </div>
</div>