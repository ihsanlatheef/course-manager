@extends('layouts.app')

@section('content')
<div class="container">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
              <div class="container">
                <div class="row">&nbsp;</div>

                <!-- Show DB results -->
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>User</th>
                        <th>Action Performed</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if (isset($records))
                      {{is_null($records)}}
                          @if(!empty($records))
                              @foreach ($records as $record)
                                  <tr>
                                      <td>{{ $record['name'] }}</td>
                                      <td>{{ $record['action'] }}</td>
                                      <td>{{ $record['date'] }}</td>
                                  </tr>
                              @endforeach
                          @unless(count($records)) 
                              <tr>
                                  <td colspan="10">No transactions have been performed on the system.</td>
                              </tr>
                          @endunless
                          @endif
                    @endif
                    </tbody>
                  </table>
                    {{ $records->links() }}
                </div>
              </div>
            @else
                @include('auth.login')
            @endif
        </div>
    @endif
</div>
@endsection