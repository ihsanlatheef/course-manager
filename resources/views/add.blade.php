@extends('layouts.app')

@section('content')
<div class="container">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
              <div class="container">
                <form method="POST" action="/employees">
                  {{ csrf_field() }}
                  <div class="panel panel-default">
                    <div class="panel panel-info" style="margin-left: 20px;margin-right: 20px;margin-top: 22px;">
                      <div class="panel-heading">
                        <h3 class="panel-title">Personal Info</h3>
                      </div>
                      <div class="panel-body panel-margin-style">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="form-group">
                                <label for="emp_id" class="control-label">Employee ID</label>
                                <input type="text" class="form-control" name="employee_id" id="emp_id">
                            </div>
                            <div class="form-group">
                                <label for="emp_unit" class="control-label">Employee Unit</label>
                                <input type="text" class="form-control" name="employee_unit" id="emp_unit">
                            </div>
                            <div class="form-group">
                                <label for="emp_hrid" class="control-label">Employee HRID</label>
                                <input type="text" class="form-control" name="employee_hrid" id="emp_hrid">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                                <label for="fullname" class="control-label">Full Name</label>
                                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="fullname">
                            </div>
                            <div class="form-group">
                                <label for="contact-no" class="control-label">Contact No</label>
                                <input type="text" class="form-control" name="contact_no" id="contact-no" placeholder="contact no">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <label for="department" class="control-label">Department</label>
                              <select class="form-control department-name" id="department" name="department">
                                <option value="PTC" selected="selected">PTC</option>
                                <option value="Civil">Civil</option>
                                <option value="HRD">HRD</option>
                                <option value="Finance">Finance</option>
                                <option value="ITD">ITD</option>
                              </select>
                            </div>
                            <div class="form-group">
                                <label for="designation" class="control-label">Designation</label>
                                <input type="text" class="form-control" name="designation" id="designation" placeholder="designation">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" style="margin-left: 0px;margin-right: 0px;padding-left: 20px;padding-bottom: 20px;">
                      <button type="submit" class="btn btn-primary">Save Employee</button>
                    </div>
                  </div>
                </form>
              </div>
            @else
                @include('auth.login')
            @endif
        </div>
    @endif
</div>
@endsection
